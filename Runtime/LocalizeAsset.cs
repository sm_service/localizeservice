﻿using System;
using mce.ConfigsLoader;
using UnityEngine;

namespace mce.LocalizeService
{
    [CreateAssetMenu(menuName = "Configs/Assets/Localize", fileName = "Localize", order = 0)]
    public class LocalizeAsset :  ConfigsAsset<LocalizeConfig>
    {
        [SerializeField] private Sprite _icon;
        [SerializeField] private SystemLanguage _language;
        public SystemLanguage Language => _language;
        public Sprite Icon => _icon;
    }
    
    [Serializable]
    public class LocalizeConfig : Config
    {
        [SerializeField] private string keyLocalize;
        [SerializeField] private string text;
        
        public string KeyLocalize => keyLocalize;
        public string Text => text;
    }
}