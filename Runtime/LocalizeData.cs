﻿using mce.LocalSave;
using UnityEngine;

namespace mce.LocalizeService
{
	public class LocalizeData : LocalData
	{
		public SystemLanguage SystemLanguage;
	}
}