﻿using System;
using System.Collections.Generic;
using System.Linq;
using mce.LocalSave;
using TMPro;
using UnityEngine;

namespace mce.LocalizeService
{
	public class LocalizeService
	{
		public event Action OnChangeLocalize;

		private readonly LocalSaveService _localSaveService;
		private readonly IReadOnlyList<LocalizeAsset> _assets;
		
		
		private Dictionary<string, string> _currentHash = new Dictionary<string, string>();
		
		private List<(SystemLanguage, TMP_Dropdown.OptionData)> _allLanguages;
		
		private LocalizeAsset _currentAsset;

		public LocalizeService(LocalSaveService localSaveService, List<LocalizeAsset> assets)
		{
			_localSaveService = localSaveService;
			_assets = assets;
			LoadLocalize();
		}
		
		private void LoadLocalize()
		{
			LocalizeData localizeData = _localSaveService.GetData<LocalizeData>();
			if (localizeData != null)
			{
				SetLanguage(localizeData.SystemLanguage);
				return;
			}

			
			foreach (var asset in _assets)
			{
				if (asset.Language == Application.systemLanguage)
				{
					localizeData = new LocalizeData();
					localizeData.SystemLanguage = Application.systemLanguage;
					_localSaveService.SaveData<LocalizeData>(localizeData);
					SetLanguage(Application.systemLanguage);
					return;
				}

			}

			localizeData = new LocalizeData();
			localizeData.SystemLanguage = SystemLanguage.English;
			_localSaveService.SaveData<LocalizeData>(localizeData);
			SetLanguage(SystemLanguage.English);
		}

		public SystemLanguage GetCurrentLanguage()
		{
			return _currentAsset.Language;
		}

		public IReadOnlyList<(SystemLanguage, TMP_Dropdown.OptionData)> GetOptionData()
		{
			if (_allLanguages != null)
				return _allLanguages;

			_allLanguages = new List<(SystemLanguage, TMP_Dropdown.OptionData)>();
			foreach (var asset in _assets)
			{
				var keyLocalize = $"localize_{asset.Language}".ToLower();
				string text = asset.Language.ToString();
				
				var config = asset.Configs.FirstOrDefault(x => x.KeyLocalize.ToLower() == keyLocalize);
				if (config != null)
				{
					text = config.Text;
				}

				_allLanguages.Add((asset.Language, new TMP_Dropdown.OptionData(text, asset.Icon)));
			}

			return _allLanguages;
		}

		public string GetLocalizeText(string key)
		{
			if (string.IsNullOrEmpty(key))
			{
				Debug.LogError($"[Localize] Localize key is empty");
				return key;
			}

			key = key.ToLower();
			if (_currentHash == null)
			{
				Debug.LogError($"[Localize] Localize not load");
				return key;
			}

			if (!_currentHash.ContainsKey(key))
			{
				var config = _currentAsset.Configs.FirstOrDefault(x => x.KeyLocalize.ToLower() == key);
				if (config == null)
				{
					Debug.LogError($"[Localize] Localize not found key: {key} Language: {_currentAsset.Language}");
					return key;
				}

				_currentHash.Add(key, config.Text);
				return config.Text;
			}
			
			return _currentHash[key];
		}

		public void SetLanguage(SystemLanguage language)
		{
			var asset = _assets.FirstOrDefault(x => x.Language == language);
			if (asset == null)
			{
				Debug.LogError($"[Localize] Not found asset for Language: {language}");
				return;
			}
			
			_currentHash.Clear();
			_currentAsset = asset;
			var newData = new LocalizeData() {SystemLanguage = language};
			_localSaveService.SaveData<LocalizeData>(newData);
			OnChangeLocalize?.Invoke();
		}
		
	}
}