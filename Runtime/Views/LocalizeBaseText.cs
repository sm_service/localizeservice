﻿using UnityEngine;

namespace mce.LocalizeService
{
	public abstract class LocalizeBaseText : MonoBehaviour
	{
		[SerializeField]
		private string _keyLocalize;

		private LocalizeService _localizeService;
		private object[] _params;
		private object param;

		
		public void SetLocalizeService(LocalizeService localizeService)
		{
			_localizeService = localizeService;
			_localizeService.OnChangeLocalize -= ResetLocalize;
			_localizeService.OnChangeLocalize += ResetLocalize;
		}

		private void OnEnable()
		{
			if(_localizeService != null)
				_localizeService.OnChangeLocalize += ResetLocalize;
		}

		private void OnDisable()
		{
			if(_localizeService != null)
				_localizeService.OnChangeLocalize -= ResetLocalize;
		}

		private void Awake()
		{
			if (!string.IsNullOrEmpty(_keyLocalize))
			{
				SetKeyLocalize(_keyLocalize);
			}
		}
		

		public void SetKeyLocalizeWithParamsLocalization(string keyFormat, params object[] args)
		{
			if(_localizeService == null)
				return;
			
			object[] localizedParams = new object[args.Length];
			for (int i = 0; i < args.Length; i++)
			{
				param = args[i];
				string localizedText = _localizeService.GetLocalizeText(param.ToString());
				if (string.IsNullOrEmpty(localizedText))
					localizedParams[i] = param;
				else
					localizedParams[i] = localizedText;
			}

			SetKeyLocalize(keyFormat, localizedParams);
		}

		public void SetKeyLocalize(string keyFormat, params object[] args)
		{
			if(_localizeService == null)
				return;
			
			_params = args;
			_keyLocalize = keyFormat;

			var textLocalize = _localizeService.GetLocalizeText(_keyLocalize);
			if (string.IsNullOrEmpty(textLocalize))
			{
				RepaintText(null);
				return;
			}
			RepaintText(string.Format(textLocalize, _params));
		}

		public void SetText(string text)
		{
			_params = null;
			_keyLocalize = null;
			RepaintText(text);
		}

		protected virtual void RepaintText(string textLocalize)
		{
		}
		
		private void ResetLocalize()
		{
			if (string.IsNullOrEmpty(_keyLocalize))
			{
				return;
			}
			if (_params == null)
			{
				SetKeyLocalize(_keyLocalize);
				return;
			}
			SetKeyLocalize(_keyLocalize, _params);
		}
	}
}