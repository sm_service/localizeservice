﻿using System.Linq;
using TMPro;
using UnityEngine;

namespace mce.LocalizeService
{
	public class LocalizeSelector: MonoBehaviour
	{
		[SerializeField]
		private TMP_Dropdown _dropdown;
		
		private LocalizeService _localizeService;
		
		public void SetLocalizeService(LocalizeService localizeService)
		{
			_localizeService = localizeService;
			Repaint();
		}

		private void OnEnable()
		{
			_dropdown.onValueChanged.AddListener(ChangeValue);
		}

		private void OnDisable()
		{
			_dropdown.onValueChanged.RemoveListener(ChangeValue);
		}

		private void Repaint()
		{
			if(_localizeService == null)
				return;
			var languages = _localizeService.GetOptionData();
			_dropdown.ClearOptions();
			_dropdown.AddOptions(languages.Select(x=>x.Item2).ToList());
			int id = 0;
			for (int i = 0; i < languages.Count; i++)
			{
				if (languages[i].Item1 == _localizeService.GetCurrentLanguage())
				{
					id = i;
					break;
				}
			}
			_dropdown.SetValueWithoutNotify(id);
		}
		
		private void ChangeValue(int id)
		{
			if(_localizeService == null)
				return;
			
			var languages = _localizeService.GetOptionData();
			_localizeService.SetLanguage(languages[id].Item1);
		}
	}
}