using TMPro;
using UnityEngine;

namespace mce.LocalizeService
{
	public class LocalizeText : LocalizeBaseText
	{
		[SerializeField]
		private TMP_Text _text;

		private void Reset()
		{
			_text = GetComponent<TMP_Text>();
		}

		protected override void RepaintText(string textLocalize)
		{
			_text.text = textLocalize;
		}
	}
}